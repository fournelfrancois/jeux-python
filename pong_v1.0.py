#!/usr/bin/env python
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#		It's my first actual game-making attempt. I know code could be much better 
#		with classes or defs but I tried to make it short and understandable with very 
#		little knowledge of python and pygame(I'm one of them). Enjoy.
#       ICI P1k4CHU
import pygame
from pygame.locals import *
from sys import exit
import random
import math

# Initialisation du moteur de jeu pygame
pygame.init()

# Création de la fenêtre de jeu taille (640,480) avec un titre
screen=pygame.display.set_mode((640,480),0,32)
pygame.display.set_caption("Pong Pong!")

# Création des éléments du jeu
# Fond d'écran couleur RGB (0,0,0)
back = pygame.Surface((640,480))
background = back.convert()
background.fill((0,0,0))

# Les 2 barres : des rectangles de taille 10x50 et de couleurs différentes
bar = pygame.Surface((10,50))
bar1 = bar.convert()
bar1.fill((0,0,255))
bar2 = bar.convert()
bar2.fill((255,0,0))

# La balle : un cercle de 15 pixels de diamètre
circ_sur = pygame.Surface((15,15))
circ = pygame.draw.circle(circ_sur,(0,255,0),(int(15/2),int(15/2)),int(15/2))
circle = circ_sur.convert()
circle.set_colorkey((0,0,0))

# Variables du jeu
# Position x et y des 2 barres 
bar1_x, bar2_x = 10. , 620.
bar1_y, bar2_y = 215. , 215.
# Position de la balle
circle_x, circle_y = 307.5, 232.5
# Vitesse de mouvement des barres (mouvement vertical uniquement)
bar1_move, bar2_move = 0. , 0.
speed_x, speed_y, speed_circ = 250., 250., 250.
# Score des 2 joueurs
bar1_score, bar2_score = 0,0

# Horloge du jeu
clock = pygame.time.Clock()
# Choix d'une fonte pour afficher du texte 
font = pygame.font.SysFont("calibri",40)

# Démarrage du jeu : boucle infinie
running = True
while running:
    # Test des évènements pour détecter les touches utilisées
    for event in pygame.event.get():
        if event.type == QUIT:
            # Appui sur la croix "fermer la fenêtre" 
            running = False

        if event.type == KEYDOWN:
            # Gestion des évènements de type "touche enfoncée"
            if event.key == K_UP:
                # K_UP = Flêche vers le haut
                bar1_move = -ai_speed
            elif event.key == K_DOWN:
                # K_DOWN = Flêche vers le bas
                bar1_move = ai_speed
                
        elif event.type == KEYUP:
            # Gestion des évènements de type touche relâchée
            if event.key == K_UP:
                bar1_move = 0.
            elif event.key == K_DOWN:
                bar1_move = 0.
            elif event.key == K_ESCAPE:
                running = False
            else:
                # Affichage de la touche appuyée pour Débug
                print(event.key)
    
    # Affichage des élements dans la fenêtre
    
    # Fond d'écran + cadre blanc + ligne au milieu
    screen.blit(background,(0,0))
    cadre = pygame.draw.rect(screen,(255,255,255),Rect((5,5),(630,470)),2)
    middle_line = pygame.draw.aaline(screen,(255,255,255),(330,5),(330,475))
    
    # Affichage des barres et de la balle 
    screen.blit(bar1,(bar1_x,bar1_y))
    screen.blit(bar2,(bar2_x,bar2_y))
    screen.blit(circle,(circle_x,circle_y))

    # Score des 2 joueurs : texte
    score1 = font.render(str(bar1_score), True,(255,255,255))
    score2 = font.render(str(bar2_score), True,(255,255,255))
    screen.blit(score1,(250.,210.))
    screen.blit(score2,(380.,210.))

    # Déplacement de la barre joueur 1 (position actuelle + vitesse)
    bar1_y += bar1_move

    # Déplacement de la balle
    time_passed = clock.tick(30)
    time_sec = time_passed / 1000.0
    
    circle_x += speed_x * time_sec
    circle_y += speed_y * time_sec
    ai_speed = speed_circ * time_sec
    
    # Déplacement de la barre de l'ordinateur en fonction de la balle.
    if circle_x >= 305.:
        if not bar2_y == circle_y + 7.5:
            if bar2_y < circle_y + 7.5:
                bar2_y += ai_speed
            if  bar2_y > circle_y - 42.5:
                bar2_y -= ai_speed
        else:
            bar2_y == circle_y + 7.5
    
    # Limite les positions des barres 1 et 2 en Y pour ne pas sortir de l'écran (min / max) 
    if bar1_y >= 420.: 
        bar1_y = 420.
    elif bar1_y <= 10. : 
        bar1_y = 10.
    if bar2_y >= 420.: 
        bar2_y = 420.
    elif bar2_y <= 10.: 
        bar2_y = 10.
    
    # Calcul du renvoi de la balle
    if circle_x <= bar1_x + 10.:
        # La balle arrive à gauche sur la barre 1 
        if circle_y >= bar1_y - 7.5 and circle_y <= bar1_y + 42.5:
            # La barre 2 est touchée, la balle rebondie 
            circle_x = 20.
            speed_x = -speed_x
    
    if circle_x >= bar2_x - 15.:
        # La balle arrive à gauche sur la barre 2 
        if circle_y >= bar2_y - 7.5 and circle_y <= bar2_y + 42.5:
            # La barre 2 est touchée, la balle rebondie 
            circle_x = 605.
            speed_x = -speed_x

    # Si la balle touche le bord de l'écran gauche ou droit : le score augmente
    if circle_x < 5.:
        # Bord gauche touché : +1 pour le joueur 2
        bar2_score += 1
        circle_x, circle_y = 320., 232.5
        bar1_y,bar_2_y = 215., 215.
    elif circle_x > 620.:
        # Bord droit touché : +1 pour le joueur 1
        bar1_score += 1
        circle_x, circle_y = 307.5, 232.5
        bar1_y, bar2_y = 215., 215.
    
    # Rebond sur le haut ou bas de l'écran
    if circle_y <= 10.:
        # Haut
        speed_y = -speed_y
        circle_y = 10.
    elif circle_y >= 457.5:
        # Bas
        speed_y = -speed_y
        circle_y = 457.5

    # Mise à jour de l'affichage du jeu (rafraichissement écran)
    pygame.display.update()
